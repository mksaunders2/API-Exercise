# API-exercise

In this task I tried keeping things simple as requested, the following are my techstack chioces and reasons why I decided to go with such.

## Techstack
***Docker***\
***Kubernetes***\
***Minikube***\
***NodeJS***\
***MongoDB***

- Docker and Kubernetes are the defacto standard for container engine and orchestration.
- Initially I used GKE but then switched to Minikube due to gcloud cred and console inaccessibilitynto others.
- I opted for NodeJS instead of Golang to use something different, I also felt it an adept tool for such task and api web services.
- MongoDB felt a match in terms of the data structures and CAP theorem.

### Setup
Run the following sequentially to get everything running\
`minikube start`\
`eval $(minikube docker-env)`\
`docker build -t api/service .`\
`kubectl create -f service.yaml`\
`kubectl create -f deployment.yaml`

Then run `minikube service api-service --url` to get the service url which is needed to make calls to the api service.

### HTTP-API
All endpoints are after `/people` resources, for example (`http://192.168.99.104:32445/people/ping`) is the `ping` endpoint.\
All other implemented rest api endpoints are described in [API.md](./API.md)

#### Caveats
I did not fill the database with csv data as mongoimports is still an issue with mongo docker.

### Other ideas not used
[Knative](https://knative.dev/)
Knative is a tool I have been exploring ultimately, it's a set of primitive components built on top of Kubernetes. It's meant to to make Kubernetes dev and ops cycle faster and easier.
I decided not go with Knative due to the dockerized database dependency factor. Knative is meant for services.






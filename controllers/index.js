const uuidv4 = require('uuid/v4');
const Person = require('../models/person');
const { validate } = require('../util');

const ping = async (_, res) => {
  res.send(`Hi I am the api service.`);
};

const getAll = async (_, res) => {
  const people = await Person.find();
  res.send(people);
};

const getOne = async (req, res) => {
  try {
    const person = await Person.findOne({ uuid:req.params.uuid });
    res.send(person.get());  
  } catch {
    res.status(204).send({});
  }
};

const save = async (req, res) => {
  const err = validate(req.body);
  if (err) {
    const message = err.details[0].message;
    res.status(400).send({ message });
    return;
  }

  const obj = { uuid: uuidv4(), ...req.body };
  const person = new Person(obj);
  await person.save();
  res.status(201).send(obj);
};

const remove = async (req, res) => {
  const person = await Person.findOne({ uuid:req.params.uuid });
  if (person) {
    await person.remove();
    res.status(204).end();
    return;
  }
  res.status(202).end();
};

const update = async (req, res) => {
  const { params: { uuid }, body } = req;

  const err = validate(req.body);
  if (err) {
    res.status(304).end();
    return;
  }
  
  const person = await Person.findOne({ uuid });
  if (person) {
    await person.set(Object.assign({}, body, { uuid }));
    await person.save();
    res.status(204).end();
    return;
  }
  res.status(304).end();
};

module.exports = {ping, getAll, getOne, save, remove, update };

const express = require('express');
const { ping, getAll, getOne, save, remove, update } = require('../controllers');
const router = express.Router();

router.get('/ping', ping);

router.get('/:uuid', getOne);

router.get('/', getAll);

router.post('/', save);

router.put('/:uuid', update);

router.delete('/:uuid', remove);

module.exports = router;

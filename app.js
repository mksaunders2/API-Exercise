const express = require('express');
const bodyParser = require('body-parser');
const { Database } = require('mongorito');

const routes = require('./routes');
const Person = require('./models/person');
require('dotenv').config();

(async function () {
  try {
    const db = new Database(process.env.DB_URL);
    await db.connect();
    db.register(Person);
  } catch (err) {
    throw new Error('Cannot connect to DB!');  }
})();

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/people', routes);

app.listen(process.env.PORT, () => {
    process.stdout.write(`Server is up and running on port number ${process.env.PORT}`);
});

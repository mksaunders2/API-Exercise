const Joi = require('@hapi/joi');
 
const schema = {
    survived: Joi.boolean().required(),
    passengerClass: Joi.number().integer().required(),
    name: Joi.string().min(3).max(30).required(),
    sex: Joi.string().alphanum().min(1).max(6).required(),
    age: Joi.number().integer().min(1).required(),
    siblingsOrSpousesAboard: Joi.number().integer().required(),
    parentsOrChildrenAboard: Joi.number().integer().required(),
    fare: Joi.number().precision(2).required(),
};

/**
 * Validates person request body object
 * 
 * @param {Object} obj
 * @returns {Object}
 */
const validate = obj => Joi.validate(obj, schema, (err, value) =>  err);

module.exports = { validate };
